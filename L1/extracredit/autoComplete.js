// this function uses the input query to get the list of city from the API of weather undergroung  
var getCityName = function(s) {
  var city_list = [];
  $.ajax( { 
    url : "http://autocomplete.wunderground.com/aq?query=".concat(s), 
    dataType : "jsonp", 
    format:"json", 
    jsonp:"cb",
    success : function(parsed_json) 
    { 
      for(var i = 0; i < parsed_json["RESULTS"].length; i++)
      {
        city_list.push(parsed_json["RESULTS"][i]["name"]);
      } 
    } 
   } );
    return city_list;
};

// this function get the user input and create the auto-complete dropdown
function update(){
  
  var x = document.getElementById("tags").value;
  var availableTags = getCityName(x);
       $( "#tags" ).autocomplete({
          source: availableTags
       });
}
