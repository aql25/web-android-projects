There are two parts of this program:

1. A java program uses HttpConnection to obtain the geographic information from Weather Undergground API.
2. HTML and JS files to create a simple auto-complete using the weather underground Auto Complete API

------------------------------------------------------------------
On the first part: 

	This program will prompt user to enter their API key to access to the weather Underground website.
	No key will be provided for user. If you do not have a API key, please go to the weather underground and sign up for one.

Files include with this part:
	WULookup.java

	In order to run the program, you need to install the gson-2.3.1jar file.

Design and Project Issues:
	The program will ask for user key to access to the weather underground geolookup API,
then it will store the return Json. The needed information will be extracted from the Json object,
and be displayed on console.

-------------------------------------------------------------------
On the second part:
	This program will use the provided autoComplete URL from weather underground API, search for the possible auto-complete words given
and display a auto-complete dropdown as the user type in the box.

Files include with this part:
	index.html
	autoComplete.js
	
	In order to run the program, you need to put the autoComplete.js in the same folder as index.html
	or manual adjust the relative path of js file in index.html

Design and Project Issues:
	This program will get the user input text as the user is typing and call a function named getCityName.
The getCityName function will concat the input user(query) to look up for possible auto0complete words from
weather underground. Then it will display all the possible words in a drop down box.



