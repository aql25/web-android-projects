package lab1;
import java.net.*;
import java.io.*;
import java.util.*;
//import org.json.*;

import com.google.gson.*;
public class WULookup {

	public static void main(String[] args)throws Exception{
		// TODO Auto-generated method stub
		// ask for key
		String key;
		if(args.length<1){
			System.out.println("Enter your own API key: ");
			Scanner in = new Scanner(System.in);
			key = in.nextLine();
		}else{
			key = args[0];
		}
		
//question 3
		System.out.println("Question 3: Print the user's zip code, city, state, lat and long \n");
		
		// connect to geolookup URL
		String URLgeolookup = "http://api.wunderground.com/api/" + key + "/conditions/geolookup/q/19104.json";
		
		URL url = new URL(URLgeolookup);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		// parse JSON from URL
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonObject rootobj = root.getAsJsonObject();
		
		// print out the zipcode, city name and state from API - geolookup
		String zipcode = rootobj.get("location").getAsJsonObject().get("zip").getAsString();
		String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
		String state = rootobj.get("location").getAsJsonObject().get("state").getAsString();
		String lat = rootobj.get("location").getAsJsonObject().get("lat").getAsString();
		String lon = rootobj.get("location").getAsJsonObject().get("lon").getAsString();
		
		System.out.println("zipcode: " + zipcode);
		System.out.println("City: " +city);
		System.out.println("State: " +state);
		System.out.println("Latitude: " +lat);
		System.out.println("Longtitude: " +lon);
		
// question 4
		System.out.println("\nQuestion 4: print the date, hour , current condition, temperature and humidity \n");
		// connect to the hourly forecast URL
		String URLhourly = "http://api.wunderground.com/api/" + key + "/conditions/hourly/q/19104.json";
		
		URL urlHourly = new URL(URLhourly);
		HttpURLConnection nextRequest = (HttpURLConnection) urlHourly.openConnection();
		nextRequest.connect();
		
		// parse Json data
		JsonParser jp1 = new JsonParser();
		JsonElement rootH = jp1.parse(new InputStreamReader((InputStream) nextRequest.getContent()));
		JsonObject rootHobj = rootH.getAsJsonObject();
		
		JsonArray array = rootHobj.get("hourly_forecast").getAsJsonArray();
		
		// get the hourly forecast
		for(int i = 0; i < array.size(); i++){
			String pretty = array.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
			String Condition = array.get(i).getAsJsonObject().get("condition").getAsString();
			String tempinF = array.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
			String humidity = array.get(i).getAsJsonObject().get("humidity").getAsString();
			System.out.println(pretty + " weather condition is " + Condition + " with temperature: " + tempinF + "F and humidity: " + humidity + "%");
		}
	}

}

