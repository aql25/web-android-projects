lab 2 - readme.txt

There are two parts of this lab:

1. Java program invokes the Google Calendar service interface using the OAuth protocol 
and obtain a list of events from the first calendar of user.

2. Java program has similar function like the first on, however, it uses Temboo Library to 
invoke Google Calendar API.

----------------------------------------------------------------------
On the first Program (GoogleCalendar.java):
	
	This program is written in Eclipse on Window 8.
	This program will prompt user to enter their client id, secret and redirect uri.
	This program will ask for user's consent and request a token from Google Calendar to
invoke the API and print a list of calendar's events.

Files include with this program:
	GoogleCalendar.java

	In order to run the program, you need to install the gson-2.3.1.jar file

Design and Project Issues:
	The program may not work probably due to the difference in the Calendar Event Json format.
In the printEvent function, this program is written based on the default JSON format and get "dateTime" 
and return the date of the event. However, in some calendars, they have different format for the JSON.
Instead of getting "dateTime", you may need to adjust it to "date" accordingly. 
Please check the Google Calendar Event Json format if any error occurs.

----------------------------------------------------------------------
On the second Program (TembooGoogleCalendar.java):

	This program is written in Eclipse on Window 8.
	This program will prompt user to enter their client id, secret and redirect uri.
	This program requires client to use Temboo Library to request data from Google API after 
getting a fresh token from Google Calendar service.

Files include with this program:
	TembooGoogleCalendar.java

	In order to run this program, you need to install Temboo_java package
and enter the Temboo account, project code.

Design and Project Issues:
	The program may not work probably due to the difference in the Calendar Event Json format.
In the printEvent function, this program is written based on the default JSON format and get "dateTime" 
and return the date of the event. However, in some calendars, they have different format for the JSON.
Instead of getting "dateTime", you may need to adjust it to "date" accordingly. 
Please check the Google Calendar Event Json format if any error occurs.
	

