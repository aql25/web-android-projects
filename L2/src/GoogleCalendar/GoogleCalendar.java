package GoogleCalendar;
import java.util.*;
import java.net.*;
import java.io.*;
import com.google.gson.*;

public class GoogleCalendar{

	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter google client id :");
		String clientid = in.nextLine();
		
		System.out.println("Enter google client secret:");
		String clientsecret = in.nextLine();
		
		System.out.println("Enter google redirect URI:");
		String redirect = in.nextLine();
		
		System.out.println("GO here: "+ requestToken(clientid, redirect));
		System.out.println("enter the code that you got: ");
		String code = in.nextLine();
		
		//use authorize code to exchange for token JSON
		String authorizeResponse = exchangeToken(clientid, redirect, clientsecret, code);
				
		// parse the return Json token and get access_token
		String token = getToken(authorizeResponse);
		
		// get the first calendar id
		String CalendarList = executeGET(requestCalendarAPI(token));
		String calendarID = getCalendarId(CalendarList, 0); 
		
		//get events of the first calendar
		String Events = executeGET(requestEvents(calendarID, token));
		printEvent(Events);
	
		in.close();
		
	}// end main
	
	// return the URL string to request Token
	public static String requestToken(String clientId, String redirectUri){
		return "https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=" 
				+ clientId + "&scope=https://www.googleapis.com/auth/calendar&response_type=code&redirect_uri=" 
				+ redirectUri + "&state=/profile&approval_prompt=force";
	}
	
	// get Token from the authorization code
	public static String exchangeToken (String clientId, String redirectUri, 
									 String clientSecret, String code){
		String authorizeURL = "https://accounts.google.com/o/oauth2/token";
		String authorizeParams = "code=" + code + "&client_id=" + clientId +
				"&client_secret=" + clientSecret + "&redirect_uri=" + redirectUri +
				"&grant_type=authorization_code";
		return executePost(authorizeURL, authorizeParams);
	}
	
	// return the url to request the calendar api
	public static String requestCalendarAPI(String token){ 
		return "https://www.googleapis.com/calendar/v3/users/me/calendarList"+
				"?access_token=" + token;
	}
	//return the url to request event from calendar events
	public static String requestEvents(String calendarID, String token){
		return "https://www.googleapis.com/calendar/v3/calendars/"+ calendarID + "/events"
				+ "?access_token=" + token;
	}

	// return token from authorization response
	public static String getToken(String authorizeResponse){
		JsonObject obj_root = parseJson(authorizeResponse);
		return obj_root.get("access_token").getAsString();
	}

	// return root JsonObject
	public static JsonObject parseJson(String json){
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(json);
		return root.getAsJsonObject();
	}
	
	// return the calendarID from the calendar list
	public static String getCalendarId(String CalendarList, int index){
		
		JsonObject obj_root = parseJson(CalendarList);

		return obj_root.get("items").getAsJsonArray().get(index).getAsJsonObject().get("id").getAsString();
	}
	
	//print all the event from the calendar
	public static void printEvent(String Events){
		
		JsonObject obj_root = parseJson(Events);
		
		JsonArray eventlist = obj_root.get("items").getAsJsonArray();		
		String time;
		
		for(int i = 0;  i < eventlist.size(); i++){
			JsonObject event = eventlist.get(i).getAsJsonObject();
			String datetime = event.get("start").getAsJsonObject().get("dateTime").getAsString();
			
			if(datetime.length() < 10){
				time = "00:00";
			}
			else{
				time = event.get("start").getAsJsonObject().get("dateTime").getAsString().substring(11,16);
			}
			
			String date = event.get("start").getAsJsonObject().get("dateTime").getAsString().substring(0,10);
			System.out.print(event.get("summary").getAsString());
			System.out.println(" / Date: " + date + " / Time: " + time );
		}
	}
	// request HTTPS POST to exchange authorization code for fresh token
		public static String executePost(String targetURL, String urlPara){
			URL url;
			HttpURLConnection connection = null;
			try{
				// create connection to target URL
				url = new URL(targetURL);
				connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				connection.setRequestProperty("Content-Length", "" +
							Integer.toString(urlPara.getBytes().length));
				connection.setRequestProperty("Content-Language", "en-US");
				
				connection.setUseCaches(false);
				connection.setDoInput(true);
				connection.setDoOutput(true);
				
				// Send Request
				DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
				wr.writeBytes(urlPara);
				wr.flush();
				wr.close();
				
				//Get Response
				
				InputStream is = connection.getInputStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				String line;
				StringBuffer response = new StringBuffer();
				while((line = rd.readLine()) != null){
					response.append(line);
					response.append('\r');
				}
				rd.close();return response.toString();
			} catch (Exception e){
				e.printStackTrace();
				return null;
			} finally {
				if(connection != null){
					connection.disconnect();
				}
			}
		}
		
		/// HTTP get request
		public static String executeGET(String targetURL){
			URL url;
			HttpURLConnection connection = null;
			try{
			url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Language", "en-US");
					
			//send request
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while((line = rd.readLine())!=null){
				response.append(line);
				response.append("\r");
			}
			rd.close();
			return response.toString();
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}finally{
				if(connection != null){
					connection.disconnect();
				}
			}
		}

}