package TembooGoogleCalendar;
import java.util.*;
import java.net.*;
import java.io.*;

import com.google.gson.*;
import com.temboo.Library.Google.*;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class TembooGoogleCalendar{

	public static void main(String[] args) throws TembooException{
		Scanner in = new Scanner(System.in);
		
		/*System.out.println("Enter google client id :");
		String clientid = in.nextLine();
		
		System.out.println("Enter google client secret:");
		String clientsecret = in.nextLine();
		
		System.out.println("Enter google redirect URI:");
		String redirect = in.nextLine();
				
		System.out.println("Enter your temboo account name: ");
		String tembooid = in.nextLine();
		
		System.out.println("Enter your temboo secret code: ");
		String temboocode = in.nextLine();*/
		String clientid = "454162506354-8csk8249pn9vp5dmhe1cdpof5rdibp7m.apps.googleusercontent.com";
		String clientsecret = "fgiHSZpfFpGf61CFFkEivjV9";
		String redirect = "https://www.example.com/oauth2callback";
		String tembooid = "kiranoyuki";
		String temboocode = "a5d4cb754fd341a3a1fcb08c95e53c61";

		TembooSession session = new TembooSession(tembooid, "myFirstApp",temboocode);
		
		// request Token 
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ClientID(clientid);
		//initializeOAuthInputs.set_ForwardingURL(redirect);
		initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar");
		
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		//get the authorization url
		String url = initializeOAuthResults.get_AuthorizationURL();
		
		//get the callback ID for temboo
		String callback = initializeOAuthResults.get_CallbackID();
		String callbackID = callback.substring(callback.indexOf("/")+1);
		System.out.println(callback);
		System.out.println(callbackID);
		System.out.println("GO here and grant your permission for this app: "+ url);
		
		System.out.println("Have you accept the app to run? please indicate Yes/No: ");
		String permission = in.nextLine();
		
		if(permission.equalsIgnoreCase("yes")||permission.equalsIgnoreCase("y")){
			String authorizeResponse = exchangeToken(session, clientid, clientsecret, callbackID);
			System.out.println(authorizeResponse);
			// get access_token
			/*String token = getToken(authorizeResponse);
				
			//get the calendar list (JSON) from google calendar API
			String response = getCalendarList(session, clientsecret, token, clientid);
			
			//parse the json and get calendar id
			String calendarID = getCalendarId(response,0);
			
			// get the list of Events (JSON) from google calendar event API
			String Events = getEvents(session,clientsecret,token, clientid,calendarID);
			
			// print all events of one calendar
			printEvent(Events);*/
		}else{
			System.out.println("Failed to run since there is no permission to access your calendar.");
		}
		
		// get authorize code 
		/*String authorizeResponse = exchangeToken(session, clientid, clientsecret, callbackID);
				
		// get access_token
		String token = getToken(authorizeResponse);
			
		//get the calendar list (JSON) from google calendar API
		String response = getCalendarList(session, clientsecret, token, clientid);
		
		//parse the json and get calendar id
		String calendarID = getCalendarId(response,0);
		
		// get the list of Events (JSON) from google calendar event API
		String Events = getEvents(session,clientsecret,token, clientid,calendarID);
		
		// print all events of one calendar
		printEvent(Events);*/
		
	}// end main
	
	
	/*	@ return the authorization code
		request the google server for an authorization code		
	*/
	public static String exchangeToken (TembooSession session, String clientId, 
									 String clientSecret, String code)throws TembooException{
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_ClientID(clientId);
		finalizeOAuthInputs.set_ClientSecret(clientSecret);
		finalizeOAuthInputs.set_CallbackID(code);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		return finalizeOAuthResults.get_AccessToken();
	}
	
	/* @ return the calendar id from a list of calendar
		index parameter to pick the wanted calendar id from the list
	*/	
	public static String getCalendarId(String CalendarList, int index){
		
		JsonObject obj_root = parseJSON(CalendarList);
		return obj_root.get("items").getAsJsonArray().get(index).getAsJsonObject().get("id").getAsString();
	}

	/* @ return JSON object 
		parse the json parameter and return json object
	*/
	public static JsonObject parseJSON (String x){
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(x);
		return root.getAsJsonObject();
	}

	/* @ return token from authorization response
		need the authorization code as parameters
	 */
	public static String getToken(String authorizeResponse){
		
		JsonObject obj_root = parseJSON(authorizeResponse);
		return obj_root.get("access_token").getAsString();
	}

	/* @ void print out the list of events
		print summary - date - time of events
		may edit "dateTime" depend on the JSON format of a particular calendar
	*/	
	public static void printEvent(String Events){
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(Events);
		JsonObject obj_root = root.getAsJsonObject();
		
		JsonArray eventlist = obj_root.get("items").getAsJsonArray();		
		String time;
		
		for(int i = 0;  i < eventlist.size(); i++){
			JsonObject event = eventlist.get(i).getAsJsonObject();
			String datetime = event.get("start").getAsJsonObject().get("dateTime").getAsString();
			
			if(datetime.length() < 10){
				time = "00:00";
			}
			else{
				time = event.get("start").getAsJsonObject().get("dateTime").getAsString().substring(11,16);
			}
			
			String date = event.get("start").getAsJsonObject().get("dateTime").getAsString().substring(0,10);
			System.out.print(event.get("summary").getAsString());
			System.out.println(" / Date: " + date + " / Time: " + time );
		}
	}

	/* @ return the list of calendar id using Temboo
	 *  
	 */
	public static String getCalendarList(TembooSession session, String clientsecret, String token, String clientid)throws TembooException{
		
		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		getAllCalendarsInputs.set_ClientSecret(clientsecret);
		getAllCalendarsInputs.set_AccessToken(token);
		getAllCalendarsInputs.set_ClientID(clientid);

		// Execute Choreo and get the id JSON
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
		return getAllCalendarsResults.get_Response();
	}
	
	/*	@ return the list of events in Json
	 * use temboo to get event from google calendar event api
	 */
	public static String getEvents(TembooSession session, String clientsecret, String token, String clientid, String calendarID)throws TembooException{
		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set inputs
		getAllEventsInputs.set_ClientSecret(clientsecret);
		getAllEventsInputs.set_AccessToken(token);
		getAllEventsInputs.set_ClientID(clientid);
		getAllEventsInputs.set_CalendarID(calendarID);

		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
		return getAllEventsResults.get_Response();
	}
	
	/*// request HTTPS POST to exchange authorization code for fresh token
		public static String executePost(String targetURL, String urlPara){
			URL url;
			HttpURLConnection connection = null;
			try{
				// create connection to target URL
				url = new URL(targetURL);
				connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				connection.setRequestProperty("Content-Length", "" +
							Integer.toString(urlPara.getBytes().length));
				connection.setRequestProperty("Content-Language", "en-US");
				
				connection.setUseCaches(false);
				connection.setDoInput(true);
				connection.setDoOutput(true);
				
				// Send Request
				DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
				wr.writeBytes(urlPara);
				wr.flush();
				wr.close();
				
				//Get Response
				
				InputStream is = connection.getInputStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				String line;
				StringBuffer response = new StringBuffer();
				while((line = rd.readLine()) != null){
					response.append(line);
					response.append('\r');
				}
				rd.close();return response.toString();
			} catch (Exception e){
				e.printStackTrace();
				return null;
			} finally {
				if(connection != null){
					connection.disconnect();
				}
			}
		}*/
	
}