Lab 4 - readme.txt

Weather Underground program:

	An app that retrieves the user location, then get the weather information of that prticular location.
Then displays all the information on screen. 
	Information is stored in sqlite database after the first call. If next call is within the first hour,
information will be extract from the database to display.

---------------------------------------------------------------------

Program details:
	Developed on Android Studio 1.0.1, Window 8
	Test on Nexus 5 Emulator, KitKat Android


Design and Project Issues:

	Program includes: 

- mySQL.java : this class handles input and output data of the SQL table.
- weather.java: this class creates weather object and its functions
- weatherAdapter.java: implements ArrayAdapters and handle parsing image_url

- table.java is not used on the new update app