package com.example.anh.weather_underground;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
/**
 * Created by Anh on 2/27/2015.
 */
public class weatherAdapter extends ArrayAdapter<weather> {

    ArrayList<weather> weatherList;
    LayoutInflater vi;
    int Resource;
    ViewHolder holder;
    Context context;

    static class ViewHolder{
        public ImageView icon;
        public TextView hour;
        public TextView date;
        public TextView description;
        public TextView temp;
        public TextView humidity;
    }

    public weatherAdapter(Context context, int resource, ArrayList<weather> objects){
        super(context, resource, objects);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        weatherList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v== null){
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.icon = (ImageView) v.findViewById(R.id.icon);
            holder.hour = (TextView) v.findViewById(R.id.hour);
            holder.date = (TextView) v.findViewById(R.id.date);
            holder.description = (TextView) v.findViewById(R.id.description);
            holder.temp = (TextView) v.findViewById(R.id.temp);
            holder.humidity = (TextView) v.findViewById(R.id.humidity);

            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }

        holder.icon.setImageResource(R.drawable.ic_launcher);
        new DownloadImageTask(holder.icon).execute(weatherList.get(position).getIcon_url());
        holder.hour.setText(weatherList.get(position).getHour());
        holder.date.setText(weatherList.get(position).getDate());
        holder.description.setText(weatherList.get(position).getDescription());
        holder.temp.setText("Temp: "+weatherList.get(position).getTemp() + "F");
        holder.humidity.setText("Humidity: " +weatherList.get(position).getHumidity());

        return v;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }

    }
}
