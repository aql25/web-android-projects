package com.example.anh.weather_underground;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anh on 3/4/2015.
 */
public class table {

    private SQLiteDatabase database;
    private mySQL db;

    private String[] cols = {mySQL.COL_KEY, mySQL.COL_HOUR, mySQL.COL_DATE,
            mySQL.COL_DES, mySQL.COL_TEMP, mySQL.COL_HU, mySQL.COL_ICON};

    public table(Context context){
        db = new mySQL(context);
    }

    public void open() throws SQLException{
        database = db.getWritableDatabase();
    }

    public void close(){
        db.close();
    }

    public void addWeather(weather w){

        Log.d("true", "addWeather of table. java is called");
        ContentValues value = new ContentValues();
        //value.put(mySQL.COL_KEY, w.getHour()+w.getDate());
        value.put(mySQL.COL_HOUR, w.getHour());
        value.put(mySQL.COL_DATE, w.getDate());
        value.put(mySQL.COL_DES, w.getDescription());
        value.put(mySQL.COL_TEMP, w.getTemp());
        value.put(mySQL.COL_HU, w.getHumidity());
        value.put(mySQL.COL_ICON, w.getIcon_url());

        database.insert(mySQL.TABLE_WEATHER, null, value);
    }

    public void upDate(weather w, int id){

        Log.d("true", "update of table. java is called");
        ContentValues value = new ContentValues();
        //value.put(mySQL.COL_KEY, w.getHour()+w.getDate());
        value.put(mySQL.COL_HOUR, w.getHour());
        value.put(mySQL.COL_DATE, w.getDate());
        value.put(mySQL.COL_DES, w.getDescription());
        value.put(mySQL.COL_TEMP, w.getTemp());
        value.put(mySQL.COL_HU, w.getHumidity());
        value.put(mySQL.COL_ICON, w.getIcon_url());

        String updateMessage = mySQL.COL_KEY+"="+id;
        database.update(mySQL.TABLE_WEATHER, value, updateMessage, null );
    }

    public Boolean sameHour(String a){
        Cursor c = database.query(mySQL.TABLE_WEATHER, new String[]{"*"},null, null, null, null, null, null);

        if(c!= null)
            if(c.moveToFirst()){

                String getKey = c.getString(1);

                Log.d(getKey, "the Key from db of samehour()");
                if(getKey.equals(a)){
                    Log.d("true", "sameHour");
                    return true;
                }

            }
        Log.d("false", "sameHour");
        return false;
    }

    public Cursor getCursorNow(String a){
        Cursor c = database.query(mySQL.TABLE_WEATHER, cols, mySQL.COL_KEY + "=?",
                new String[]{a}, null, null, null, null);
        return c;
    }


    public weather getWeather(Cursor c){
        weather wu = new weather();
        wu.setHour(c.getString(1));
        wu.setDate(c.getString(2));
        wu.setDescription(c.getString(3));
        wu.setTemp(c.getString(4));
        wu.setHumidity(c.getString(5));
        wu.setIcon_url(c.getString(6));

        return wu;
    }


    public List<weather> getAllWeather(){
        List<weather> list = new ArrayList<weather>();
        String selectQuery = "SELECT * FROM " + mySQL.TABLE_WEATHER;
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();


        while(!c.isAfterLast()){
            weather wu = getWeather(c);
            list.add(wu);
            c.moveToNext();
        }

        c.close();
        return list;

    }
}
