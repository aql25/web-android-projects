package com.example.anh.weather_underground;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anh on 3/4/2015.
 */
public class mySQL extends SQLiteOpenHelper {

    public static final String TABLE_WEATHER = "weathers";
    public static final String COL_KEY = "key";
    public static final String COL_HOUR= "hour";
    public static final String COL_DATE = "date";
    public static final String COL_DES = "description";
    public static final String COL_TEMP = "temp";
    public static final String COL_HU = "humidity";
    public static final String COL_ICON = "icon_url";

    private static final String DATABASE_CREATE = "create table " + TABLE_WEATHER + "("
            + COL_KEY + " integer auto increment," + COL_HOUR + " text not null,"+ COL_DATE + " text not null,"
            +COL_DES + " text not null,"+ COL_TEMP+ " text not null," + COL_HU +" text not null,"
            + COL_ICON + " text not null)";

    private static final String DATABASE_NAME = "weather.db";
    private static final int VERSION = 3;


    public mySQL(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_WEATHER);
        onCreate(db);
    }

    // adding new weather
    public void addWeather(weather w,int i){
        SQLiteDatabase db = this.getReadableDatabase();

        Log.d("true", "addWeather ");
        ContentValues value = new ContentValues();
        value.put(COL_KEY, Integer.toString(i));
        value.put(COL_HOUR, w.getHour());
        value.put(COL_DATE, w.getDate());
        value.put(COL_DES, w.getDescription());
        value.put(COL_TEMP, w.getTemp());
        value.put(COL_HU, w.getHumidity());
        value.put(COL_ICON, w.getIcon_url());

        db.insert(mySQL.TABLE_WEATHER, null, value);

        db.close();
    }
    // get a single weather
    public void getWeather(int id){
        String a[] = new String[]{COL_KEY,COL_HOUR, COL_DATE, COL_DES, COL_TEMP, COL_HU, COL_ICON};
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_WEATHER, a, COL_KEY+"=?", new String[]{String.valueOf(id)},
                null, null, null, null);

        if(c!=null) {
            c.moveToFirst();
            Log.d(c.getString(0), "id first row");
            Log.d(c.getString(1), "hour of first row");
        }
        Log.d("null", "cursor in getWeather");



    }

    //update a single weather

    public void updateWeather(weather w, int i){

        Log.d("true", "updateWeather is called");
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues value = new ContentValues();
        value.put(COL_KEY, Integer.toString(i));
        value.put(COL_HOUR, w.getHour());
        value.put(COL_DATE, w.getDate());
        value.put(COL_DES, w.getDescription());
        value.put(COL_TEMP, w.getTemp());
        value.put(COL_HU, w.getHumidity());
        value.put(COL_ICON, w.getIcon_url());

        db.update(TABLE_WEATHER, value, COL_KEY+"= ?", new String[]{String.valueOf(i)});
    }

    //get all weather
    public List<weather> getAllWeather(){
        List<weather> list = new ArrayList<weather>();
        String selectQuery = "SELECT * FROM " + mySQL.TABLE_WEATHER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if(c.moveToFirst()){
            do{
                weather wu = new weather();
                wu.setHour(c.getString(1));
                wu.setDate(c.getString(2));
                wu.setDescription(c.getString(3));
                wu.setTemp(c.getString(4));
                wu.setHumidity(c.getString(5));
                wu.setIcon_url(c.getString(6));

                list.add(wu);
            }while(c.moveToNext());
        }

        c.close();
        return list;

    }
    // check sameHour
    public Boolean sameHour(String a){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(mySQL.TABLE_WEATHER, new String[]{"*"},null, null, null, null, null, null);

        if(c!= null)
            if(c.moveToFirst()){

                String getKey = c.getString(1);

                Log.d(getKey, "the Key from db of samehour()");
                if(getKey.equals(a)){
                    Log.d("true", "sameHour");
                    return true;
                }

            }
        Log.d("false", "sameHour");
        return false;
    }


}
