package com.example.anh.weather_underground;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class MainActivity extends ActionBarActivity {

    ArrayList<weather> weatherList;
    weatherAdapter adapter;
    ListView list;
    mySQL db;
    boolean needUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherList = new ArrayList<weather>();
        list = (ListView) findViewById(R.id.list_view);
        needUpdate = false;

        // create database
        db = new mySQL(this);

        weatherList = (ArrayList<weather>) db.getAllWeather();
        Log.d(Integer.toString(weatherList.size()), "first weatherList size");

        if(weatherList.size() == 0){
            new weatherAsyncTask().execute();
        }else{
            if(!db.sameHour(getTime())){
                needUpdate = true;
                new weatherAsyncTask().execute();
            }else{
                weatherAdapter adapter = new weatherAdapter(getApplicationContext(), R.layout.list_row, weatherList);
                list.setAdapter(adapter);
            }
        }


    }

    class weatherAsyncTask extends AsyncTask<Void, Void, Boolean>{

        ProgressDialog pd;
        protected void OnPreExecute(){
            super.onPreExecute();
            /*pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Loading..");
            pd.setTitle("Connecting..");
            pd.show();
            pd.setCancelable(false);*/
        }
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                weatherList.clear();
                //Log.d("Asynctask", "executed");
                String APIkey = "8106f22d453bf108";
                String geoIP = "http://api.wunderground.com/api/" + APIkey + "/geolookup/q/autoip.json";

                // look up geoip
                URL url = new URL(geoIP);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.connect();
                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                JsonObject rootobj = root.getAsJsonObject();

                String geo = rootobj.get("location").getAsJsonObject().get("zip").getAsString();


                String URLhourly = "http://api.wunderground.com/api/" + APIkey + "/conditions/hourly/q/"+ geo +".json";
                URL urlHourly = new URL(URLhourly);
                HttpURLConnection nextRequest = (HttpURLConnection) urlHourly.openConnection();
                nextRequest.connect();

                // parse Json data
                JsonParser jp1 = new JsonParser();
                JsonElement rootH = jp1.parse(new InputStreamReader((InputStream) nextRequest.getContent()));
                JsonObject rootHobj = rootH.getAsJsonObject();

                JsonArray array = rootHobj.get("hourly_forecast").getAsJsonArray();

                // get the hourly forecast
                for (int i = 0; i < 24 ; i++) {

                    JsonObject a = array.get(i).getAsJsonObject();
                    weather weatherObj = new weather();

                    weatherObj.setHour(a.get("FCTTIME").getAsJsonObject().get("civil").getAsString());
                    weatherObj.setTemp(a.get("temp").getAsJsonObject().get("english").getAsString());
                    weatherObj.setHumidity(a.get("humidity").getAsString());
                    weatherObj.setIcon_url(a.get("icon_url").getAsString());
                    weatherObj.setDescription(a.get("condition").getAsString());
                    weatherObj.setDate(a.get("FCTTIME").getAsJsonObject().get("mon").getAsString()+"/" + a.get("FCTTIME").getAsJsonObject().get("mday").getAsString()
                            +"/" + a.get("FCTTIME").getAsJsonObject().get("year").getAsString());

                    weatherList.add(weatherObj);
                    if(needUpdate){
                        db.updateWeather(weatherObj, i);
                        Log.d(Integer.toString(i), "weather update");
                    }else{
                        db.addWeather(weatherObj, i);
                    }
                }
                db.getWeather(0);

                return true;

            }catch (ClientProtocolException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;


        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == true){
                weatherAdapter adapter = new weatherAdapter(getApplicationContext(), R.layout.list_row, weatherList);
                Log.d(Integer.toString(weatherList.size()), "weather list size on post execute");
                list.setAdapter(adapter);

            }else{
                System.out.print("error in update UI");
            }
        }

    }

    public String getTime(){

        DateFormat a = new SimpleDateFormat("a");
        DateFormat hour = new SimpleDateFormat("h");

        String Hour = hour.format(Calendar.getInstance().getTime());
        String A = a.format(Calendar.getInstance().getTime());

        int nextHour = Integer.parseInt(Hour) + 1;

        String test = Integer.toString(nextHour) + ":00 " + A;

        Log.d(test, "Log out getTime()");

        return test;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
