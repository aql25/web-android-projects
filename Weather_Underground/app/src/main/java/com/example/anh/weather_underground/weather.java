package com.example.anh.weather_underground;

/**
 * Created by Anh on 2/27/2015.
 */
public class weather {

    private String hour;
    private String date;
    private String description;
    private String temp;
    private String humidity;
    private String icon_url;

    public weather(){};

    public weather(String hour, String date, String description, String temp, String humidity,
                   String icon_url){
        this.hour = hour;
        this.date = date;
        this.description = description;
        this.temp = temp;
        this.humidity = humidity;
        this.icon_url = icon_url;
    }

    public String getHour(){
        return hour;
    }
    public String getDate(){
        return date;
    }
    public String getDescription(){
        return description;
    }
    public String getTemp(){
        return temp;
    }
    public String getHumidity(){
        return humidity;
    }
    public String getIcon_url(){
        return icon_url;
    }

    public void setHour(String hour){
        this.hour = hour;
    }
    public void setDate(String date){
        this.date = date;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setTemp(String temp){
        this.temp = temp;
    }
    public void setHumidity(String humidity){
        this.humidity = humidity;
    }
    public void setIcon_url(String icon_url){
        this.icon_url = icon_url;
    }

}
