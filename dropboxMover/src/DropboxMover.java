import java.util.*;
import java.net.*;
import java.io.*;
import java.io.BufferedWriter;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.dropbox.core.*;


public class DropboxMover {

	
	public static void main(String[] args) throws TembooException{
		// TODO Auto-generated method stub
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		
//Getting information to set up the app
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter your appID: ");
		String appID = in.nextLine();
		
		System.out.println("Enter your appSecret: ");
		String appSecret = in.nextLine();
		
		System.out.println("Enter your temboo ID: ");
		String tembooID = in.nextLine();
		
		System.out.println("Enter your temboo key: ");
		String tembooKey = in.nextLine();
		
		// set up the Choreo for Temboo
		TembooSession session = new TembooSession(tembooID, "myFirstApp" , tembooKey);
				
// Set up the inittializeOAuth in Temboo and Request the token of Dropbox
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_DropboxAppKey(appID);
		initializeOAuthInputs.set_DropboxAppSecret(appSecret);
		
		// Execute Choreo
		// Receive the url to ask for user permission, callback ID and OAuth Token secret
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		String url = initializeOAuthResults.get_AuthorizationURL();
		String callbackID = initializeOAuthResults.get_CallbackID();
		String OAuthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
		
		
		System.out.println("Grant us access at : " + url);
	
		
// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_DropboxAppKey(appID);
		finalizeOAuthInputs.set_DropboxAppSecret(appSecret);
		finalizeOAuthInputs.set_CallbackID(callbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(OAuthTokenSecret);
		
		// Execute Choreo
		// Receive Access Token, Access Token Secret, and userID
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		String AccessToken = finalizeOAuthResults.get_AccessToken();
		String AccessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		String userID = finalizeOAuthResults.get_UserID();
		
			
// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
// Use temboo to get the file content	
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_AccessToken(AccessToken);
		getFileInputs.set_AccessTokenSecret(AccessTokenSecret);
		getFileInputs.set_AppKey(appID);
		getFileInputs.set_AppSecret(appSecret);
		getFileInputs.set_EncodeFileContent("false");
		
		// Get the list of files and directories 		
		String __list =  getFile("move/__list.txt",getFileChoreo,getFileInputs);			
		String[] list = __list.split("\n");
		
		/*  
		    Use for loop to read the name file and directory
			Then use function getFile() to copy the file's content
			Create new text file in its destination with the previous file's content
		*/ 
		for(int i = 0; i < list.length; i++){
			
			// read the file name and directory
			String[] Listline = list[i].split(" ");
			String nameFile = Listline[0];
			String directory = Listline[1];
			String path = directory.trim() + "/" + nameFile.trim();
			
			// Copy the file content from dropbox
			String text = getFile("move/" + nameFile, getFileChoreo, getFileInputs);
			
			// Copy the text file to its destination
			try{
				File textFile = new File(path);
				BufferedWriter file = new BufferedWriter(new FileWriter(textFile));
				
				String[] lines = text.split("\n");
				for(String line: lines ){
					file.write(line);
					file.newLine();
				}
				file.close();	
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		
		System.out.println("Files are copied successfully from Dropbox!");
				
	}
	
	/* 
	  return the content of file in dropbox
	  need directory of the needed file
	*/ 
	public static String getFile(String Path, GetFile getFileChoreo, GetFileInputSet getFileInputs)throws TembooException{
		
		//set path to get file
		getFileInputs.set_Path(Path);
				
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		return getFileResults.get_Response();
		
	}
	
	

}
