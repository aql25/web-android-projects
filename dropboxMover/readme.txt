Assignment 1 - readme.txt

- This program searchs your Dropbox entries for files in a directory called "move". It reads
a file called "__list" to move files as specifying in __list files.

- This program is written in Eclipse on Window 8.
- This program uses Temboo to acquire the Dropbox authorization token. It will prompt user for their 
app id, secret and temboo id, temboo key.
- This program will ask for user's consent, then uses the token to invoke Dropbox API.
- It reads the _list file and continuosly copy the files in the same directory with _list.
Then create new text files in the instructed destination of each file. 

Files include with this program:
	DropboxMover.java

Design and Project Issues:
	Because this program uses Temboo to call the getFile API, you need to be careful about how many
calls you are allowed to use on Temboo. Each file on Dropbox will need one call from Temboo to copy the file's content.
If the total number of calls are excess the limit of Temboo calls, program may crash.

	I decided to use a for loop to read through the _list, look up the needed file's content and create new file in its destination.
For some reason, when I split the string of each line in the _list file, I need to trim the string (delete new line or whitespce char)
in order to create new file in the right directory.

