Practicum lab - CS275 README.txt

This program calculates the grade level required to read tweet from a given twitter account
with SMOG grade level.
This program connects to the Twitter API user Timeline and downloads a default 30 tweets from 
a given user account. Then it outputs each polysyllabic word and counts the number of polysyllable words.
And outputs the SMOG grade level.

Files included in this program:
	checkSMOG.java
	
	To run this program, you need to include google gson-2.3.1 and temboo sdk 2.7.0

	The program will prompt you for: client id and secret, temboo id and key, wordnik API key
	Program is written in Eclipse on Windows 8.

Design and Issues:

	The default number of tweets to be download is 30. It calls a function named temboo()
and connects with twitter OAuth through Temboo and returns a Json list of tweets. It will get the tweet from
user's timeline when they log in into twitter and allows the program to run.

	This Program returns the number of sentences EXACTLY as the number of sentences in every tweet. It means that
I do not assume each tweet only has one sentence but can be multiple sentences in a tweet.
So dont be surprise when you see the total sentences more than expectation when testing it. I split the string with [.?!:] to determine when a sentence ends.
 