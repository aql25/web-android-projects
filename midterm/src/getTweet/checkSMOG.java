package getTweet;

import java.util.*;
import java.net.*;
import java.io.*;

import com.google.gson.*;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import com.temboo.Library.Twitter.OAuth.*;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;

public class checkSMOG {

	static final Scanner systemInScanner = new Scanner(System.in);

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		String response = temboo(30);
		
		Scanner in = systemInScanner;
		System.out.println("last but not least, your WordNit API key: ");
		String APIkey = in.nextLine();
		
		ArrayList<String> test = getTweetArray(response);
		
		// initialize the number of polySyllables and number of Sentences
		int wordsCount = 0;
		int sentenceCount = 0;
		
		System.out.println("List of PolySyllable words:");
		
		//check number of polySyllable words and number of Sentences
		for(int i = 2; i < test.size();i++){
			
			// trim quotation mark and any leading or trailing whitespace of each tweet
			String trimmed = ((String) test.get(i)).replaceAll("(\u201c|\u201d)", " ").trim();
			
			// split tweet into words and check for polySyllable in a tweet
			for(String a : trimmed.split("\\s+")){
				
				//only get the alphabetical word, remove special character
				a = a.replaceAll("[.*\\W+.*]", "");
				
				// check for polySyllable word
				if(a != null && !a.isEmpty() && checkPolyS(a,APIkey)){
					
					System.out.println(a);
					wordsCount++;
				}
			}			
			// split tweet into sentence and count number of sentences in a tweet
			int count = trimmed.split("[!?.:]+").length;			
			if(count >0){
				sentenceCount += count;
			}			
		}// end for loop
		
		System.out.println("num of polyword: " + wordsCount);
		System.out.println("num of sentence: " + sentenceCount);
	
		double grade = 1.0430 * Math.sqrt((double)wordsCount *(30.00/(double)sentenceCount)) + 3.1291;
		
		System.out.println("level of SMOG is : " + grade);
		in.close();
	}
	
	/*	@ return the JSON of user timeline information
	 * Use Temboo OAuth to do get authorization token on twitter 
	 * Use Temboo userTimeline to get user timeline info with the given access token
	 * require consumerKey, consumerSecret, temboo account and key parameters
	 */
	public static String temboo(int NumOfTweets) throws TembooException{
		
		Scanner in = systemInScanner;
		System.out.println("This program use temboo to connect with Twitter.");
		System.out.println("I need you client key: ");
		String consumerKey = in.nextLine();
		
		System.out.println("I need your client secret: ");
		String consumerSecret = in.nextLine();
		
		System.out.println("How about your temboo id?");
		String tembooID = in.nextLine();
		System.out.println("Your temboo key too: ");
		String tembooKey = in.nextLine();
		
// Initialize the OAUTH using Temboo
		
		TembooSession session = new TembooSession(tembooID, "myFirstApp", tembooKey);

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ConsumerKey(consumerKey);
		initializeOAuthInputs.set_ConsumerSecret(consumerSecret);
		
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		// get output from Choreo
		String AuthorizationURL = initializeOAuthResults.get_AuthorizationURL();
		String CallbackID = initializeOAuthResults.get_CallbackID();
		String OAuthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
		
		System.out.println("Go here: " + AuthorizationURL);
		
// Finalize OAUTH using Temboo
		
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_ConsumerKey(consumerKey);
		finalizeOAuthInputs.set_ConsumerSecret(consumerSecret);
		finalizeOAuthInputs.set_CallbackID(CallbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(OAuthTokenSecret);
		
		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		//get output from Choreo
		String AccessToken = finalizeOAuthResults.get_AccessToken();
		String AccessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		String ScreenName = finalizeOAuthResults.get_ScreenName();
		String UserID = finalizeOAuthResults.get_UserID();
		
// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ScreenName(ScreenName);
		userTimelineInputs.set_AccessToken(AccessToken);
		userTimelineInputs.set_AccessTokenSecret(AccessTokenSecret);
		userTimelineInputs.set_ConsumerSecret(consumerSecret);
		//userTimelineInputs.set_UserId(UserID);
		userTimelineInputs.set_ConsumerKey(consumerKey);
		userTimelineInputs.set_Count(NumOfTweets);
		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		
		String response = userTimelineResults.get_Response();
		
		//System.out.println(response);
		return response;		
	}
	
	/* @ return a list of tweets on user timeline
	 * parse the twitter user timeline Json and store the tweets in an arraylist
	 */
	public static ArrayList<String> getTweetArray(String responseJson){
		
		JsonParser jp = null;
		JsonElement root = null;
		JsonArray rootH = null;
		try {
			
			jp = new JsonParser();
			root = jp.parse(responseJson);
			rootH = root.getAsJsonArray();
		}catch (Exception e){
			e.printStackTrace();
		}		
		
		ArrayList<String> test = new ArrayList<String>();
		for(int i = 0; i <  rootH.size(); i++){
			String text = rootH.get(i).getAsJsonObject().get("text").getAsString();
			test.add(text);
		}
		return test;
	}
	/* @ return number of syllables of the given word 
	 * connect to WordNit API and get the return Hyphenation in JSON
	 * require word parameter and WordNit API key
	 */
	public static int getnumSylables(String word, String APIkey)throws Exception{
		
		String wordnikURL = "http://api.wordnik.com/v4/word.json/" + word + 
				"/hyphenation?useCanonical=true&limit=50&api_key=" + APIkey ;
		
		URL url = new URL(wordnikURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		// parse JSON from URL
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonArray rootobj = root.getAsJsonArray();
		
		return rootobj.size();		
	}
	
	/* @return true if the given word is a polySyllable word
	 * require word to check and WordNit API key
	 */
	public static boolean checkPolyS(String word, String APIkey) throws Exception{
		int numOfSylables = getnumSylables(word,APIkey);
		
		if(numOfSylables >= 3){
			return true;
		}
		return false;		
	}

}
