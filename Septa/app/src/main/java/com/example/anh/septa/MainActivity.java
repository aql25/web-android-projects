package com.example.anh.septa;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    ArrayList<train> trainList;
    List<String> placeList;
    ListView listView;
    Button search;
    String sData;
    String dData;


    static final LatLng CENTER = new LatLng(39.9500, -75.1667);
    private GoogleMap map;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // intialize elements
        placeList = new ArrayList<String>();

        // get all possible source/destination for auto complete
        new AutoCompleteTask().execute();

        search = (Button) findViewById(R.id.searchB);
        listView = (ListView) findViewById(R.id.listView);

        // search button
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trainList = new ArrayList<train>();
                sData = ((AutoCompleteTextView) findViewById(R.id.sourceInput)).getText().toString();
                dData = ((AutoCompleteTextView) findViewById(R.id.desInput)).getText().toString();

                if(!sData.trim().equals("") && !sData.trim().equals("")){

                    String nextURL = "http://www3.septa.org/hackathon/NextToArrive/" + sData.trim().replaceAll(" ", "%20") +
                            "/" + dData.trim().replaceAll(" ", "%20") + "/20";

                    Log.d(nextURL, "getTrainList URL");
                    new nextToArrive().execute(nextURL);

                }else{
                    Toast.makeText(getApplicationContext(), "Input is invalid",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        //click on train list
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(position >= 0){
                    String trainId = ((TextView) view.findViewById(R.id.id)).getText().toString();
                    trainInfoTask trainInfo = new trainInfoTask(getApplicationContext());
                    trainInfo.execute(trainId);

                    return true;
                }else{
                    Log.d(Integer.toString(position), "position onIntemLongClick");
                    Log.d("false", "trainInfo executed");
                }
                return false;
            }
        });

        //show map
        View getMapButton = (Button) findViewById(R.id.getMapButton);
        getMapButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                setContentView(R.layout.activity_maps);
                map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
                if(map == null){
                    Toast.makeText(getApplicationContext(), "Map cannot found",
                            Toast.LENGTH_LONG).show();
                }else{

                    trainMapTask trainMap = new trainMapTask(getApplicationContext(), map);

                    trainMap.execute();
                    // Let the user see indoor maps where available.
                    map.setIndoorEnabled(true);

                    // Enable my-location stuff
                    map.setMyLocationEnabled(true);

                    // Move the "camera" (view position) to our center point.
                    map.moveCamera(CameraUpdateFactory.newLatLng(CENTER));
                    // Then animate the markers while the map is drawing,
                    // since you can't combine motion and zoom setting!
                    final int zoom = 10;
                    map.animateCamera(CameraUpdateFactory.zoomTo(zoom), 1500, null);
                }


            }
        });



    }

    // get next to arrive list of train
    class nextToArrive extends AsyncTask<String, Void, Boolean>{

        @Override
        protected Boolean doInBackground(String... params) {

            try{
                // look up the next to Arrive trains
                String link = params[0];
                URL url = new URL(link);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.connect();

                // parse the list train
                JsonParser jp = new JsonParser();
                JsonElement root =  jp.parse(new InputStreamReader((InputStream) request.getContent()));
                JsonArray rootArray = root.getAsJsonArray();

                for(int i = 0; i < rootArray.size(); i++){
                    JsonObject a = rootArray.get(i).getAsJsonObject();
                    train newTrain = new train();
                    newTrain.setId(a.get("orig_train").getAsString());
                    newTrain.setTimeD(a.get("orig_departure_time").getAsString());
                    newTrain.setTimeA(a.get("orig_arrival_time").getAsString());
                    newTrain.setStatus(a.get("orig_delay").getAsString());

                    // add into the list
                    trainList.add(newTrain);
                }

                Log.d(Integer.toString(trainList.size()), "size of the trainList");
                request.disconnect();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean == false){
                Toast.makeText(getBaseContext(),"Cannot get data of train", Toast.LENGTH_LONG).show();
            }else{
                if(trainList.size() > 0){
                    trainAdapter adapter = new trainAdapter(getApplicationContext(), R.layout.list_row, trainList);
                    listView.setAdapter(adapter);
                    Log.d(Integer.toString(trainList.size()), "train list size on post execute");
                }else{
                    Toast.makeText(getBaseContext(),"No train on this route is available this time", Toast.LENGTH_LONG).show();
                }

            }

        }
    }

    // function parsing cvs file to get sources
    class AutoCompleteTask extends AsyncTask <Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {

                BufferedReader br = null;
                String urlCvs = "http://www3.septa.org/hackathon/Arrivals/station_id_name.csv";
                String line = "";
                int count = 0;

                URL _url = new URL(urlCvs);
                HttpURLConnection nRequest = (HttpURLConnection) _url.openConnection();
                nRequest.connect();

                br = new BufferedReader(new InputStreamReader((InputStream) nRequest.getContent()));

                while ((line = br.readLine()) != null) {
                    String[] place = line.split(",");

                    if (count != 0) {
                        placeList.add(place[1]);
                        //System.out.println(place[1]);
                    }
                    count++;
                }
                Log.d(Integer.toString(placeList.size()), "placeLIst size");

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, placeList);
            AutoCompleteTextView sInput = (AutoCompleteTextView) findViewById(R.id.sourceInput);
            sInput.setAdapter(adapter1);
            Log.d("true", "autocomplete");

            ArrayAdapter<String>  adapter2 = new ArrayAdapter<String> (getApplicationContext(), android.R.layout.simple_list_item_1, placeList);
            AutoCompleteTextView dInput = (AutoCompleteTextView) findViewById(R.id.desInput);
            dInput.setAdapter(adapter2);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
