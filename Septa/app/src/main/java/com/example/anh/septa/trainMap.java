package com.example.anh.septa;

/**
 * Created by Anh on 3/11/2015.
 */
public class trainMap {

    private String lat;
    private String lon;

    public trainMap(){};
    public trainMap(String lat, String lon){
        this.lat = lat;
        this.lon = lon;
    };

    public void setLat(String lat){
        this.lat = lat;
    }
    public void setLon(String lon){
        this.lon = lon;
    }

    public String getLat(){
        return lat;
    }
    public String getLon(){
        return lon;
    }


}
