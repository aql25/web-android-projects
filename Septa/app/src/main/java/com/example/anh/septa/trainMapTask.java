package com.example.anh.septa;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anh on 3/11/2015.
 */


// function to get the train information
public class trainMapTask extends AsyncTask<Void, Void, Boolean> {

    private Context mContext;
    List<trainMap> listTrain;
    GoogleMap mapTrain;
    private String info;
    public trainMapTask(Context context, GoogleMap map){
        mContext = context;
        mapTrain = map;
    }


    @Override
    protected Boolean doInBackground(Void... params) {

        try{
            listTrain = new ArrayList<trainMap>();

            String urlTrain = "http://www3.septa.org/hackathon/TrainView/";
            Log.d(urlTrain, "urlTrain for longclick");

            URL url = new URL(urlTrain);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            Log.d("true", "urlTrain connected");
            // parse the list train
            JsonParser jp = new JsonParser();
            JsonElement root =  jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonArray rootArray = root.getAsJsonArray();
            Log.d(Integer.toString(rootArray.size()), "trainInfo array");

            for(int i = 0; i < rootArray.size() ; i++){
                trainMap train = new trainMap();
                train.setLat(rootArray.get(i).getAsJsonObject().get("lat").getAsString());
                train.setLon(rootArray.get(i).getAsJsonObject().get("lon").getAsString());

                listTrain.add(train);
            }

            return true;

        }catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(aBoolean == false){
            Log.d("false", "trainInfoTask");
            Toast.makeText(mContext, "Cannot retrieve data from TrainView", Toast.LENGTH_LONG).show();
        }else{

            for(int i = 0; i < listTrain.size(); i++){
                LatLng location = new LatLng(Double.parseDouble(listTrain.get(i).getLat()), Double.parseDouble(listTrain.get(i).getLon()));
                Marker a = mapTrain.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.train)));
            }


        }
    }


}




