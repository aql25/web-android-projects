package com.example.anh.septa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Anh on 3/9/2015.
 */
public class trainAdapter extends ArrayAdapter<train> {

    ArrayList<train> trainList;
    LayoutInflater vi;
    int Resource;
    ViewHolder holder;
    Context context;

    static class ViewHolder{
        public TextView id;
        public TextView timeD;
        public TextView timeA;
        public TextView status;
    }

    public trainAdapter(Context context, int resource, ArrayList<train> list){
        super(context, resource, list);
        vi = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        trainList = list;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v== null){
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.id = (TextView) v.findViewById(R.id.id);
            holder.timeD = (TextView) v.findViewById(R.id.timeD);
            holder.timeA = (TextView) v.findViewById(R.id.timeA);
            holder.status = (TextView) v.findViewById(R.id.status);

            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }

        holder.id.setText(trainList.get(position).getId());
        holder.timeD.setText(trainList.get(position).getTimeD());
        holder.timeA.setText(trainList.get(position).getTimeA());
        holder.status.setText(trainList.get(position).getStatus());

        return v;
    }
}
