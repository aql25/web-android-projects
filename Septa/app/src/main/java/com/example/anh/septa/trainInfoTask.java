package com.example.anh.septa;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Anh on 3/10/2015.
 */

// function to get the train information
public class trainInfoTask extends AsyncTask<String, Void, Boolean> {

    private Context mContext;
    private String info;
    public trainInfoTask(Context context){
        mContext = context;
    }

    @Override
    protected Boolean doInBackground(String... params) {

        try{
            int count = 0;
            boolean found = false;
            String trainId = params[0];
            String urlTrain = "http://www3.septa.org/hackathon/RRSchedules/" + trainId;
            Log.d(urlTrain, "urlTrain for longclick");

            URL url = new URL(urlTrain);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            Log.d("true", "urlTrain connected");
            // parse the list train
            JsonParser jp = new JsonParser();
            JsonElement root =  jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonArray rootArray = root.getAsJsonArray();
            Log.d(Integer.toString(rootArray.size()), "trainInfo array");

            while(count < rootArray.size() && !found){
                String time = rootArray.get(count).getAsJsonObject().get("act_tm").getAsString();
                Log.d(time, "time of next");
                if(checkTime(time)){
                    found = true;
                    Log.d("true", "checkTime");
                    if(count!=0) {
                        // do stuff here
                        String station = rootArray.get(count - 1).getAsJsonObject().get("station").getAsString();
                        String schedule = rootArray.get(count - 1).getAsJsonObject().get("sched_tm").getAsString();
                        String real = rootArray.get(count - 1).getAsJsonObject().get("act_tm").getAsString();

                        Log.d(station, "station");
                        Log.d(schedule, "schedule");
                        Log.d(real, "real");
                        info = "Train number: " + trainId + "Passed station " + station + ", schedule time: "
                                + schedule + ", arrivale at: " + real;
                        Log.d(info, "train info");

                        return true;
                    }
                }
                count++;
                Log.d(Integer.toString(count), "count");
            }

        }catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(aBoolean == false){
            Log.d("false", "trainInfoTask");
            Toast.makeText(mContext, "No previous data for this train is available now", Toast.LENGTH_LONG ).show();
        }else{
            Log.d("true", "trainInfoTask");
            Toast.makeText(mContext, info, Toast.LENGTH_LONG ).show();
        }
    }


    public boolean checkTime(String time){

        if(time.equals("na")){
            return true;
        }
        return false;
    }


}



