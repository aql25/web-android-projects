package com.example.anh.septa;

/**
 * Created by Anh on 3/9/2015.
 */
public class train {

    private String id;
    private String timeD;
    private String timeA;
    private String status;

    public train(){};
    public train(String id, String timeD, String timeA, String status){
        this.id = id;
        this.timeD = timeD;
        this.timeA = timeA;
        this.status = status;

    }

    public String getId(){
        return id;
    }
    public String getTimeD(){
        return timeD;
    }
    public String getTimeA(){
        return timeA;
    }
    public String getStatus(){
        return status;
    }

    public void setId(String id){
        this.id = id;
    }
    public void setTimeD(String timeD){
        this.timeD = timeD;
    }
    public void setTimeA(String timeA){
        this.timeA = timeA;
    }
    public void setStatus(String status){
        this.status = status;
    }


}
