Assignment 2 - readme.txt

Septa App:

	This app prompt user to input the source and destination that user wants to go.
Then it displays all the trains that will go from the source to the expected destination around the time user uses the app.
	If user long press on the itme of train list, it will display the previous station, arrival time of the train.
	On click on the mapp button will display a map with train's location at that time. 

-----------------------------------------------------------------

Program details:
	Develop on Android STudio 1.0.1, Window 8
	Test on Nexus 5 Emulator, Android Lollipop 


Design and Project Issues:

	Program Includes:

-train: a class to store basic info of the train list
-trainMap: a class to store longtitude and latitude of trains that appear on TrainView web.
-trainAdapter: retrieves info and display UI of the train list
-trainInfoTask: Asynctask to get the train info on long click
-trainMapTask: retrieves trains' location and display on google map