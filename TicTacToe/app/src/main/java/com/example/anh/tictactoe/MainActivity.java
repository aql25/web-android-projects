package com.example.anh.tictactoe;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements OnClickListener {

    Button b1,b2,b3,b4,b5,b6,b7,b8,b9, newGame;
    Button[] btnArray;
    // use turn to keep track of X and O turn
    // X = "true", O = "false"
    boolean turn = true;
    int count= 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button) findViewById(R.id.x1);
        b2 = (Button) findViewById(R.id.x2);
        b3 = (Button) findViewById(R.id.x3);
        b4 = (Button) findViewById(R.id.y1);
        b5 = (Button) findViewById(R.id.y2);
        b6 = (Button) findViewById(R.id.y3);
        b7 = (Button) findViewById(R.id.z1);
        b8 = (Button) findViewById(R.id.z2);
        b9 = (Button) findViewById(R.id.z3);
        newGame = (Button) findViewById(R.id.reset);

        btnArray = new Button[]{b1,b2,b3,b4,b5,b6,b7,b8,b9};

        // register event listener for each button
        // this class implement the OnClickListener interface
        // that contains the OnClick()
        for(Button b: btnArray){
            b.setOnClickListener(this);
        }
        // set different onclick for new game Button
        newGame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // reset all value
                turn = true;
                count = 0;
                Enable_Disable_Btn(true);
            }
        });
    }


    @Override
    public void onClick(View v){
        Button b = (Button) v;
        clickBtn(b);
    }

    public void clickBtn(Button b){

        // set text on click
        if(turn){
            b.setText("X");
        }else{
            b.setText("O");
        }

        // change background color and clickable
        b.setBackgroundColor(Color.parseColor("#DADFE1"));
        b.setClickable(false);

        // switch turn
        turn = !turn;
        count++;

        //check for winner
        checkWin();
    }

    public void checkWin(){
       boolean ifWin = false;

        //check horizontally
        if(b1.getText() == b2.getText() && b2.getText() == b3.getText() && !b1.isClickable()){
            ifWin = true;
        }else if(b4.getText() == b5.getText() && b4.getText() == b6.getText() && !b4.isClickable()){
            ifWin = true;
        }else if(b7.getText() == b8.getText() && b8.getText() == b9.getText() && !b7.isClickable()){
            ifWin = true;
        }
        //check vertically
        if(b1.getText() == b4.getText() && b4.getText() == b7.getText() && !b1.isClickable()){
            ifWin = true;
        }else if(b2.getText() == b5.getText() && b5.getText() == b8.getText() && !b2.isClickable()){
            ifWin = true;
        }else if(b3.getText() == b6.getText() && b6.getText() == b9.getText() && !b3.isClickable()){
            ifWin = true;
        }
        //check diagonally
        if(b1.getText() == b5.getText() && b5.getText() == b9.getText() && !b1.isClickable()){
            ifWin = true;
        }else if(b3.getText() == b5.getText() && b5.getText() == b7.getText() && !b5.isClickable()){
            ifWin = true;
        }

        if(ifWin){
            if(!turn){
                toast("X wins!");
            }else{
                toast("O wins!");
            }
            // set remaining buttons to gray and unclickable
            Enable_Disable_Btn(false);

        }else if (count == 9){
            toast("No winner in this game!");
        }
    }
    public void Enable_Disable_Btn(boolean enable){
        for(Button b : btnArray){
            b.setClickable(enable);

            if(enable){
                b.setBackgroundColor(Color.parseColor("#F62459"));
                b.setText("");
            }else{
                b.setBackgroundColor(Color.parseColor("#DADFE1"));
            }
        }


    }

    public void toast(String mess){
        Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
