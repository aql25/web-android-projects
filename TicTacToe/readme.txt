lab 3 - readme.txt

Tic tac toe program:

	A game on Android that allows user to play the tic tac toe game.

------------------------------------------------------------------
Program details:

	This program is developed on Android Studio, Window 8.
	Test on Nexus 5 Emulator, Lollipop Android.

Design and Project Issues:
	
	User clicks the button to play the game. X or O will be marked on each turn.
The program will check the winner and display who won. After it finds a winner, the remaining
buttons will be disable.
	New Game button available to reset the game.




